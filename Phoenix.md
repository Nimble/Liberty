# Phoenix

## Small Index

## Library/Glossary

This section is designed to keep a coherence in this piece of information and warn linguistic confusions (Of agreement to Ludwig Wittgenstein).

1. **Mind**:

2. **Qualia**:

3. **Tautology**: A redundant statement of truth that relies on itself or another to validate its veracity. Example: The color of the sky is blue because it is blue. These aren’t so present as a top or common level of language and not even mathematics but they are abundant in mathematical axioms.

## Why does this exists? Why this creed? Nothing is real, Everything is permitted

As it stands now, in the current moment, we are already one of the worst situations regarding the diversity of perspectives, because there is one set or main vertient, which is the scientific materialistic reductive understatement of reality, on other words the atomist world. In a mental sense then of all the possibilities a world can have and “exist” on the agreement of our reality is a material reality based on atoms, this is what we agree is real on a unconscious mental layer.

The problem with this is that such a world is difficult to change or reimagine with the mind, and for that very reason it is extremely difficult to see or understand it in another way without falling into fallacies or bluntly denying physical reality. This paradigm is redundant and full of tautologies and arbitrary truths, which isn’t the worst part, many can be like this and still being open about other cosmogonies, the most dangerous issue isn’t that it is hard to change, but that it has become the hegemony of thought, the central dogma to reality, and as such nothing else stays true, but that.

Thus is my will and my duty to change this pessimistic paradigm, this is the purpose of this text, to change your perspective of the world, this text will get into your head, into your mind, will play with your own unconscious, I will give you MY very own personal vision of the world, and in the end I will let you draw your own conclusions, but make no mistake, I don’t claim this to be reality, nor existence, my saying and anything I write here is not absolute, for the simple reason that I am not absolute, I do this as a man, in a body of flesh very much like you reading this.

“What was it like before?” There was no before, I am just fixing what it is now. The best version I can think and imagine with our current set of rules of the present. I am talking fully in this section about the epistemological conceptions and implications of the world we currently live.

I want you to think of a question, what life do we live now? What do you experience as life? Think deep on this, if you do it superficial you will surf through experience, never really getting it nor what you are doing.

My answer to this is the perspective on my own ego in first instance, what’s my ego you may ask? It is what I understand as myself, it is what constitutes who I am in this world, it is who I am here mentally, it is an archetype everyone has and what psychology commonly understands as an individual, thinking also this is the whole extension of the being which really isn’t. So what is the ego? It is me who is writing this, it is the archetype that answers your name when someone ask for my identity, this is merely the surface of what I truly am which I will get you to understand in the next pages.

The ego isn’t the body, but it is the mental archetype more closely related to it, the ego is purely a mental construct and as such it is plastic and ever-changing as well.

So now going back to the question, life is what the ego experiences, from where I initially acquire control of my body as a baby until my death, all my experiences compose what I understand as real, and what I see, my own perspective, my own senses, however my senses as yours have been standardized from a early age, they have been categorized and our thoughts discarded and scrambled, since a young age we have been told how to thought and how the world works, not by innate direct experience, but by other people experiences held as absolute truths, wherever you take your eyes you will see this, parents, religion, schools, science, all dogmas to life but barely any that helps in the construction of your ego, they aren’t opinions, they are truths to be held by the infant mind.

And that’s only the beginning into the indoctrination.

This work, this scripture is a revolutionary act, it is the ashes, it is the ember of thought, of what was burned when we adopted our current paradigm, it is its own dark and black shadow, it is a new way of thinking and even of acquiring knowledge.

This is the war on logos, and this is how I win it. It is the end to all preconceived mental structures, it is a guide of thought never forgetting it is my own mind and my own thought what is conducing you, it is a dialogue between your mind an mine, in this very moment as you read this, you can’t talk yet because you have been muted, but I intend to change that because I want to hear you, I want to listen to authentic voices.

The worship of praxis, the worship of “evil”, I have seen it, I have seen what a war on logos can do because I have involved myself in one before, I didn’t know, I made me forget what never happened, my mind was in shambles not knowing what to believe in, maybe the nature or form of this reality is different for this very reason, but why should you believe in anything?

I mean this reality, this world forces you to believe in itself, it is sustained mentally by the very form of your and mine experience, you are a body that holds or sustains a consciousness somehow, from this our conception of the world arises, but you are not alone far before your parents and their parents passed through the same, and from this raised the material perception of reality of existence itself, what is real is what you can experience with your body, this is what has been told from the perspective of other bodies, but this is just because the tool or the vehicle for experience in this world is a body, restricting consciousness and reality to only what can be sensed through it is something that I consider a mistake, there is so much more beyond your body and mine, while should you or anyone restrict themselves? Do not answer, do not seek this question, it will only bring you despair.

In truth my will is the liberation of the minds, I don’t like any creed, I don’t like any structure, I prefer and choose the individual over anything, and for that very reason you should never take anything I say as a truth very much less your own truth, you must not become me, nor adopt the ways of my mind, I am not your teacher, I am not a Buddha, I am not a Christ, I am not a Saint, I am me, I am a man with a Daimon, I am a mage that wants you the individual to become yourself in the purest form, and for that there is no guide, only one that can make your path is yourself and beings related in one way or another to you, I am here to show you how is done, by doing it myself in this writing, in my solving on the maze that is the mind and reality in this world.

Logos: Why should anything matter?

### Brands (and their involvement with the mind)

### Usefulness (You need to be productive to belong in society)

### Money – Into the machine (Why do we do stuff? what’s the incentive to live? Why can’t we just be and have fun?)

## Dreams Beyond Control (What will be of the future)

## The problem of will (Where is it? Why can’t I just do stuff?)
